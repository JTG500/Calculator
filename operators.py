import math, re

def add(frame, text_box):

    frame.updateText(text_box, "+")

def sub(frame, text_box):

    frame.updateText(text_box, "-")

def mult(frame, text_box):

    frame.updateText(text_box, "*")

def div(frame, text_box):

    frame.updateText(text_box, "/")

def exp(frame, text_box):

    frame.updateText(text_box, "**")

def root(frame, text_box):

    frame.updateText(text_box, "sqrt(")

def period(frame, text_box):

    frame.updateText(text_box, ".")

def lpar(frame, text_box):

    frame.updateText(text_box, "(")

def rpar(frame, text_box):

    frame.updateText(text_box, ")")

def clear(frame, text_box):

    frame.updateText(text_box, clearAll=True)

def backspace(frame, text_box):

    frame.updateText(text_box, clear=True)

def mod(frame, text_box):

    frame.updateText(text_box, "%")

def one(frame, text_box):

    frame.updateText(text_box, "1")

def two(frame, text_box):

    frame.updateText(text_box, "2")

def three(frame, text_box):

    frame.updateText(text_box, "3")

def four(frame, text_box):

    frame.updateText(text_box, "4")

def five(frame, text_box):

    frame.updateText(text_box, "5")

def six(frame, text_box):

    frame.updateText(text_box, "6")

def seven(frame, text_box):

    frame.updateText(text_box, "7")

def eight(frame, text_box):

    frame.updateText(text_box, "8")

def nine(frame, text_box):

    frame.updateText(text_box, "9")

def zero(frame, text_box):

    frame.updateText(text_box, "0")

def equals(frame, text_box):

    sqroot_pat = re.compile(r"sqrt\([^\)]+\)")
    sqroot_num = re.compile(r"(\d)")
    text = frame.getText(text_box)
    sqroot = re.search(sqroot_pat, text)
    if sqroot is not None:
        length = sqroot.span()
        sqroot_value = float(re.search(sqroot_num, sqroot.group(0)).group(0))
        sqroot_value = str(math.sqrt(sqroot_value))
        if length[0] > 0:
            text = text[:length[0]]+'*'+sqroot_value+text[length[1]::]
        else:
            text = text[:length[0]]+sqroot_value+text[length[1]::]

    try:
        value = eval(text)
        clear(frame, text_box)
        frame.updateText(text_box, value)
    except SyntaxError:
        clear(frame, text_box)
        frame.updateText(text_box, "ERROR")
    except TypeError:
        num_pat = re.compile(r"\d\(")
        num_text = re.search(num_pat, text)
        if num_text is not None:
            length = num_text.span()
            re_text = num_text.group(0)[0]+'*'+num_text.group(0)[1]
            text = text[:length[0]]+re_text+text[length[1]::]
        value = eval(text)
        clear(frame, text_box)
        frame.updateText(text_box, value)
    except NameError:
        clear(frame, text_box)
