from tkinter import *

class Window:

    def __init__(self):

        self.window = Tk()
        self.frames = {}
        self.screen_width = self.window.winfo_screenwidth()
        self.screen_height = self.window.winfo_screenheight()
        self.screen_size = str(self.screen_width)+'x'+str(self.screen_height)
        self.buttons = {}
        self.texts = {}
        self.resizable = True

    def changeResizable(self):

        if self.resizable == True:
            self.resizable = False
            self.window.resizable(False, False)
        else:
            self.resizable = True
            self.window.resizable(True, True)

    def createButton(self, frame=None, button_name=None, button_text=None, function=None):

        if frame is None:
            frame = self.window
        if button_name is None:
            button_name = "Button"+str(len(self.buttons))
        if function is None:
            print (frame)
            self.buttons[button_name] = Button(frame, text=button_text)
        else:
            self.buttons[button_name] = Button(frame, text=button_text, command=function)

    def positionButtons(self, stacked=False):

        x = 1
        y = 0.1
        if stacked == True:
            for button in self.buttons.values():
                button.place(relx=x-0.1, rely=y+0.1)
                y += 0.1
                if y > 0.5:
                    y = 0.1
                    x -= 0.1

    def positionFrames(self):

        self.frames["Operators"].place(relx=0, rely=1)

    def modifySizeButtons(self, width, height):

        for button in self.buttons.values():
            button.configure(height=height, width=width)

    def createFrame(self, frame_name):

        self.frames[frame_name] = Frame(self.window)

    def createText(self, frame=None, width=0, height=0, text_name=None, size="12"):

        if frame is None:
            frame = self.window
        if width == 0:
            width = self.screen_width
        if height == 0:
            height = self.screen_height-self.screen_width

        if text_name is None:
            text_name = str(len(self.texts))
        self.texts[text_name] = Text(frame, height=height, width=width)


    def createWindow(self, title=None, screen_width=None, screen_height=None):

        if screen_width is not None:
            self.screen_width = screen_width
        if screen_height is not None:
            self.screen_height = screen_height
        if screen_height and screen_width is not None:
            self.screen_size = str(screen_width)+'x'+str(screen_height)
        if title is not None:
            self.window.title(title)
        self.window.geometry(self.screen_size)
        self.window.mainloop()

    def loadButton(self, button_name, position=None):

        if position is None:
            self.buttons[button_name].pack()
        else:
            self.buttons[button_name].pack(side=position)

    def loadButtons(self, side=None):

        index = 0
        for button in self.buttons.values():
            if side is not None:
                if side[index] == "RIGHT":
                    button.pack(side=RIGHT)
                elif side[index] == "LEFT":
                    button.pack(side=LEFT)
                elif side[index] == "BOTTOM":
                    button.pack(side=BOTTOM)
            index += 1

    def loadFrame(self, frame):

        frame.pack()

    def loadAllFrames(self):

        for frame in self.frames.values():
            frame.pack()

    def loadText(self, text_name):

        self.texts[text_name].pack()

    def updateText(self, text_name, text="Add text to me", clear=False, clearAll=False):

        if clear == False and clearAll==False:
            self.texts[text_name].insert(INSERT, text)
        elif clearAll == True:
            self.texts[text_name].delete("1.0", END)
        elif clear == True and clearAll == False:
            self.texts[text_name].delete("insert -1 chars", "insert")

    def configureText(self, text_name, font):

        self.texts[text_name].configure(font=font)

    def getText(self, text_name):

        return self.texts[text_name].get("1.0", END)
