import calculator

def main():

    calculator.makeCalculatorScreen()
    calculator.makeFrames()
    calculator.makeButtons()
    calculator.makeCalculator()

if __name__ == '__main__':
    main()
