from window import Window
import operators as op

window = Window()

def makeFrames():

    window.createFrame("Numbers")
    window.createFrame("Operators")
    window.loadAllFrames()
    window.positionFrames()

def makeButtons():

    operatorFrame = window.frames["Operators"]
    numbersFrame = window.frames["Numbers"]
    #window.createButton(frame=operatorFrame, button_name="Add", button_text="+", function=lambda:op.add(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="Subtract", button_text="-", function=lambda:op.sub(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="Multiply", button_text="*", function=lambda:op.mult(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="Divide", button_text="/", function=lambda:op.div(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="Exponent", button_text="exp", function=lambda:op.exp(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="SQRoot", button_text="sqrt", function=lambda:op.sqrt(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="Period", button_text=".", function=lambda:op.period(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="LPar", button_text="(", function=lambda:op.lpar(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="RPar", button_text=")", function=lambda:op.rpar(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="Clear", button_text="C", function=lambda:op.clear(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="Backspace", button_text="<-", function=lambda:op.backspace(window, "CalculatorScreen"))
    #window.createButton(frame=operatorFrame, button_name="Modulo", button_text="%", function=lambda:op.mod(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="1", button_text="1", function=lambda:op.one(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="2", button_text="2", function=lambda:op.two(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="3", button_text="3", function=lambda:op.three(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="4", button_text="4", function=lambda:op.four(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="5", button_text="5", function=lambda:op.five(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="6", button_text="6", function=lambda:op.six(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="7", button_text="7", function=lambda:op.seven(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="8", button_text="8", function=lambda:op.eight(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="9", button_text="9", function=lambda:op.nine(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="0", button_text="0", function=lambda:op.zero(window, "CalculatorScreen"))
    #window.createButton(frame=numbersFrame, button_name="=", button_text="=", function=lambda:op.equals(window, "CalculatorScreen"))


    window.createButton(button_name="Add", button_text="+", function=lambda:op.add(window, "CalculatorScreen"))
    window.createButton(button_name="Subtract", button_text="-", function=lambda:op.sub(window, "CalculatorScreen"))
    window.createButton(button_name="Multiply", button_text="*", function=lambda:op.mult(window, "CalculatorScreen"))
    window.createButton(button_name="Divide", button_text="/", function=lambda:op.div(window, "CalculatorScreen"))
    window.createButton(button_name="Exponent", button_text="exp", function=lambda:op.exp(window, "CalculatorScreen"))
    window.createButton(button_name="SQRoot", button_text="sqrt", function=lambda:op.root(window, "CalculatorScreen"))
    window.createButton(button_name="Period", button_text=".", function=lambda:op.period(window, "CalculatorScreen"))
    window.createButton(button_name="LPar", button_text="(", function=lambda:op.lpar(window, "CalculatorScreen"))
    window.createButton(button_name="RPar", button_text=")", function=lambda:op.rpar(window, "CalculatorScreen"))
    window.createButton(button_name="Clear", button_text="C", function=lambda:op.clear(window, "CalculatorScreen"))
    window.createButton(button_name="Backspace", button_text="<-", function=lambda:op.backspace(window, "CalculatorScreen"))
    window.createButton(button_name="Modulo", button_text="%", function=lambda:op.mod(window, "CalculatorScreen"))
    window.createButton(button_name="1", button_text="1", function=lambda:op.one(window, "CalculatorScreen"))
    window.createButton(button_name="2", button_text="2", function=lambda:op.two(window, "CalculatorScreen"))
    window.createButton(button_name="3", button_text="3", function=lambda:op.three(window, "CalculatorScreen"))
    window.createButton(button_name="4", button_text="4", function=lambda:op.four(window, "CalculatorScreen"))
    window.createButton(button_name="5", button_text="5", function=lambda:op.five(window, "CalculatorScreen"))
    window.createButton(button_name="6", button_text="6", function=lambda:op.six(window, "CalculatorScreen"))
    window.createButton(button_name="7", button_text="7", function=lambda:op.seven(window, "CalculatorScreen"))
    window.createButton(button_name="8", button_text="8", function=lambda:op.eight(window, "CalculatorScreen"))
    window.createButton(button_name="9", button_text="9", function=lambda:op.nine(window, "CalculatorScreen"))
    window.createButton(button_name="0", button_text="0", function=lambda:op.zero(window, "CalculatorScreen"))
    window.createButton(button_name="=", button_text="=", function=lambda:op.equals(window, "CalculatorScreen"))


    window.loadButtons()
    window.modifySizeButtons(4,2)
    window.positionButtons(stacked=True)


def makeCalculatorScreen():

    window.createText(text_name="CalculatorScreen", width=window.screen_width, height=window.screen_height/500)
    window.loadText("CalculatorScreen")
    window.configureText("CalculatorScreen", font=("Verdana", 28))

def makeCalculator():

    window.changeResizable()
    window.createWindow("My Calculator", 400, 600)
