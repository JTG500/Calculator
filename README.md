# Calculator

Created a basic wrapper to tkinter and a simple calculator

## Calculator app using Python and tkinter

## Description
This calculator was created using python 3 with libraries such as tkinter, math, re. The main class of the code includes functions that wrap the tkinter library and create a more streamlined interface for it. 

## Installation
To install simply clone the repository and activate a conda environment using the environment_droplet.yml file.
